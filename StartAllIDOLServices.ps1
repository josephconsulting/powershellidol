﻿$licenseServer = Get-Service | Where-Object {$_.Name -eq "MicroFocus-LicenseServer"}
Start-Service $licenseServer

$services = Get-Service | Select * | Where-Object {$_.Name -match 'MicroFocus'}
$services | Start-Service -Verbose