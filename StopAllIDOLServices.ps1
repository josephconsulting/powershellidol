﻿$services = Get-Service | Select * | Where-Object {$_.Name -match 'MicroFocus'}

$services | Stop-Service -ErrorAction SilentlyContinue