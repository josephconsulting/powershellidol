﻿Stop-Service -DisplayName "MicroFocus-CFS-Dell" -Confirm:$false
ps cfs -ErrorAction SilentlyContinue | kill -PassThru -Confirm:$false
Stop-Service -DisplayName "MicroFocus-FileSystemConnector" -Confirm:$false
ps filesystemconnector -ErrorAction SilentlyContinue | kill -PassThru -Confirm:$false
Write-Host "Deleting files"
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\cfs2\cfs\failed\* -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\cfs2\cfs\actions\* -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\cfs2\cfs\logs\* -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\cfs2\cfs\temp\* -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\actions\* -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\logs\* -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\temp\* -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\connector_DELLDOCS_datastore.db -Force

Start-Service -DisplayName "MicroFocus-CFS-Dell"
Start-Service -DisplayName "MicroFocus-FileSystemConnector"