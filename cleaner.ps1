﻿Get-Service -DisplayName 'MicroFocus-CFS' | Stop-Service

Get-Service -DisplayName 'HP MicroFocus-SocialMediaConnector' | Stop-Service

Remove-Item "C:\MicroFocus\IDOLServer-11.5.0\cfs\actions\*" -Recurse
Remove-Item "C:\MicroFocus\IDOLServer-11.5.0\cfs\logs\*" -Recurse
Remove-Item "C:\MicroFocus\IDOLServer-11.5.0\cfs\IDXArchive\*" -Recurse
Remove-Item "C:\Program Files\HP\SocialMediaConnector\actions\*" -Recurse
Remove-Item "C:\Program Files\HP\SocialMediaConnector\logs\*" -Recurse
Remove-Item "C:\Program Files\HP\SocialMediaConnector\*" -Recurse -include .db

Get-Service -DisplayName 'MicroFocus-CFS' | Start-Service

Get-Service -DisplayName 'HP MicroFocus-SocialMediaConnector' | Start-Service
