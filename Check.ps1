﻿Stop-Service -DisplayName "MicroFocus-CFS-FS" -Confirm:$false
ps cfs -ErrorAction SilentlyContinue | kill -PassThru -Confirm:$false
Stop-Service -DisplayName "MicroFocus-FileSystemConnector" -Confirm:$false
ps filesystemconnector -ErrorAction SilentlyContinue | kill -PassThru -Confirm:$false
Write-Host "Deleting files"
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\failed\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\actions\*  | Remove-Item  -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\logs\*  | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\temp\*  | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\*.lck  | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\indextemp\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\CFS-FS\cfs\outgoing\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\actions\*  | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\logs\*  | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\temp\*  | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\filesystemconnector\*.db  | Remove-Item -Recurse -Force

Start-Service -DisplayName "MicroFocus-CFS-FS"
Start-Service -DisplayName "MicroFocus-FileSystemConnector"