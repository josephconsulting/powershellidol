﻿Write-Host "Clearing the Database"
Invoke-WebRequest -Uri http://localhost:9101/DREDELDBASE?DREDbName=CFC_India


Write-Host "Stopping Services"
Get-Service -DisplayName 'MicroFocus-CFS' | Stop-Service -Force
ps cfs -ErrorAction SilentlyContinue | kill -PassThru
Get-Service -DisplayName 'MicroFocus-WebConnector' | Stop-Service -Force
ps webconnector -ErrorAction SilentlyContinue | kill -PassThru

Write-Host "Deleting files"
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs\actions\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs\logs\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs\*.idx | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs\failed\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs\indextemp\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs\temp\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs\outgoing\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\webconnector\actions\* | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\webconnector\logs\*.* | Remove-Item  -Recurse -Force
Remove-Item C:\MicroFocus\IDOLServer-11.6.0\cfs\cfs.lck -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\webconnector\*.db | Remove-Item -Recurse -Force
Get-ChildItem C:\MicroFocus\IDOLServer-11.6.0\webconnector\temp\* | Remove-Item -Recurse -Force


Write-Host "Starting Services"
Get-Service -DisplayName 'MicroFocus-CFS' | Start-Service
ps cfs
Get-Service -DisplayName 'MicroFocus-WebConnector' | Start-Service
ps webconnector

